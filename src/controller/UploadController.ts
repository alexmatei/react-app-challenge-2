import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { generateAccessToken } from "../utils/tokenManager";
import toArrayBuffer from "../utils/fileUpload/fileUploadConveter";
import predictionStep from "../image_recognition/script.js";

export class UploadController {
    async evaluate(request: Request, response: Response, next: NextFunction) {
        if(request.files == null || request.files.image == null) {
            response.status(400);
            return JSON.parse(`{"message" : "Bad Request! No image resource found!"}`);
        }

        try {
            response.status(200);
            return await predictionStep(toArrayBuffer(request.files.image.data));
        } catch(err) {
            response.status(400);
            return JSON.parse(`{"message": "Unexpected Error Occured: ${err}"}`);
        }
    }
}


