import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { generateAccessToken, verifyAccessToken } from "../utils/tokenManager";

export class UserController {
  private userRepository = getRepository(User);

  async all(request: Request, response: Response, next: NextFunction) {
    return this.userRepository.find();
  }

  async one(request: Request, response: Response, next: NextFunction) {
    let requestedUser = new User();
    try {
      requestedUser = await this.userRepository.findOne(request.params.id);
    } catch (err) {
      response.status(404);
      return "User not found";
    }
    response.status(200);
    return requestedUser;
  }

  async update(request: Request, response: Response, next: NextFunction) {
    try {
      this.userRepository.update(request.params.id, request.body);
    } catch (err) {
      response(409);
      return `Error when trying to update user with username ${request.body.username} \n ${err}`;
    }
    response.status(200);
    return request.body;
  }

  async save(request: Request, response: Response, next: NextFunction) {
    let createdUser = this.userRepository.save(request.body);
    response.status(201);
    return createdUser;
  }

  async remove(request: Request, response: Response, next: NextFunction) {
    let userToRemove = await this.userRepository.findOne(request.params.id);
    try {
      await this.userRepository.remove(userToRemove);
    } catch (err) {
      response.status(404);
      return "This user could not be found!";
    }
    response.status(200);
    return JSON.parse(
      `{"message" : "The selected user was successfully deleted!"}`
    );
  }

  async login(request: Request, response: Response, next: NextFunction) {
    let existingUserCredentials = await this.userRepository.findOne({
      where: { email: request.body.email, password: request.body.password },
    });
    if (!existingUserCredentials) {
      response.status(401);
      return JSON.parse(`{"message" : "Invalid credentials!"}`);
    }
    const token = generateAccessToken(
      existingUserCredentials.email,
      existingUserCredentials.id
    );
    const updatedUser = JSON.parse(`{
        "id" : "${existingUserCredentials.id}",
        "email" : "${existingUserCredentials.email}",
        "password" : "${existingUserCredentials.password}",
        "username" : "${existingUserCredentials.username}",
        "age" : "${existingUserCredentials.age}",
        "token" : "${token}"
    }`);
    await this.userRepository.update(existingUserCredentials.id, updatedUser);
    response.status(200);
    return JSON.parse(`{"token" : "${token}"}`);
  }

  async register(request: Request, response: Response, next: NextFunction) {
    let existingUserEmail = await this.userRepository.findOne({
      where: { email: request.body.email },
    });
    if (existingUserEmail) {
      response.status(409);
      return "This user already exists!";
    }
    let existingUsername = await this.userRepository.findOne({
      where: { username: request.body.username },
    });
    if (existingUsername) {
      response.status(409);
      return "This user already exists!";
    }
    this.userRepository.save(request.body);
    response.status(201);
    return `User ${request.body.username} created successfully!`;
  }
}
