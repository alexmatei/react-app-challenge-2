import { UploadController } from "./controller/UploadController";
import { UserController } from "./controller/UserController";
import { GCPController } from "./controller/GCPController";
import {
  validateLoginInput,
  validateRegisterInput,
} from "./middleware/inputValidation";
import { verifyAccessToken } from "./utils/tokenManager";

export const Routes = [
  {
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
  },
  {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middleware: [verifyAccessToken],
  },
  {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middleware: [verifyAccessToken],
  },
  {
    method: "put",
    route: "/users/:id",
    controller: UserController,
    action: "update",
    middleware: [verifyAccessToken],
  },
  {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middleware: [verifyAccessToken],
  },
  {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login",
    middleware: [validateLoginInput],
  },
  {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register",
    middleware: [validateRegisterInput],
  },
  {
    method: "post",
    route: "/upload",
    controller: UploadController,
    action: "upload",
    middleware: [verifyAccessToken],
  },
  {
    method: "post",
    route: "/train",
    controller: UploadController,
    action: "train",
    middleware: [verifyAccessToken],
  },
  {
    method: "post",
    route: "/evaluate",
    controller: UploadController,
    action: "evaluate",
  },
  {
    method: "get",
    route: "/getSchedule",
    controller: GCPController,
    action: "getSchedule",
  },
  {
    method: "post",
    route: "/postSchedule",
    controller: GCPController,
    action: "postSchedule",
  },
];
