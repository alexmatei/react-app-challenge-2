import "reflect-metadata";
import { createConnection } from "typeorm";
import * as cors from 'cors';
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";


createConnection().then(async connection => {

    // add cors middleware 
    const allowCrossOrigins = ['http://localhost:3001'];
    const reqOptions: cors.CorsOptions = {
        origin: allowCrossOrigins
    }

    // create express app
    const app = express();

    app.use(reqOptions)
    app.use(bodyParser.json());

    var router = express.Router();

    // register express routes from defined application routes
    Routes.forEach(route => {

        if (route.middleware) {
            route.middleware.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware)
            });
        }
        (router as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...
    app.use('/', router);
    // start express server
    app.listen(3001);


    console.log("Express server has started on port 3001. Open http://localhost:3001/users to see results");

}).catch(error => console.log(error));
