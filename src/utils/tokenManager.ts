import { NextFunction, Request, Response } from "express";
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

export function generateAccessToken(email: string, userId: number) {
    return jwt.sign({ email, userId }, process.env.TOKEN_SECRET as string, {
        expiresIn: "1h",
    });
}

export async function verifyAccessToken(
    request: Request,
    response: Response,
    next: NextFunction
) {
    if (request.headers.authtoken) {
        const authToken = request.headers.authtoken;
        const verifiedToken = jwt.verify(
            authToken,
            process.env.TOKEN_SECRET as string,
            (err, user) => {
                if (err) {
                    return response
                        .status(403)
                        .send("Unknown error occured, auth failed!");
                }
                request.userData = verifiedToken;
                next();
            }
        );
    } else {
        response.status(401).send("Access denied!");
    }
}
