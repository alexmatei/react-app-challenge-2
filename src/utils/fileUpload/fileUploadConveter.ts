export default function toArrayBuffer(buffer: String) {
    var arrayBuffer = new ArrayBuffer(buffer.length);
    var view = new Uint8Array(arrayBuffer);
    
    for(var index = 0; index < buffer.length; ++index) {
        view[index] = Number(buffer[index]);
    }

    return arrayBuffer;
} 