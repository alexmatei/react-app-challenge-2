export const MODEL_DIR = 'file://./src/image_recognition/model';
export const MODEL_PATH = 'file://./src/image_recognition/model/model.json';


export default function toArrayBuffer(buffer) {
    var arrayBuffer = new ArrayBuffer(buffer.length);
    var view = new Uint8Array(arrayBuffer);
    
    for(var index = 0; index < buffer.length; ++index) {
        view[index] = Number(buffer[index]);
    }

    return arrayBuffer;
} 