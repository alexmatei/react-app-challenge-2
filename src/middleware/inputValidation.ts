import { NextFunction, Request, Response } from "express";
import { body, validationResult } from 'express-validator';

export async function validateLoginInput(
  request: Request,
  response: Response,
  next: NextFunction
) {
  if (!body('email')) {
    return response.status(400).send("Please specify an email!");
  } else if (!request.body.password) {
    return response.status(400).send("Please specify a password!");
  }

  if (!body('email').isEmail()) {
    return response.status(400).send("Please enter a valid email!");
  }

  return next();
}

export async function validateRegisterInput(
  request: Request,
  response: Response,
  next: NextFunction
) {
  if (!body('email')) {
    return response.status(400).send("Please specify an email!");
  } else if (!request.body.password) {
    return response.status(400).send("Please specify a password!");
  }

  if (!body('email').isEmail()) {
    return response
      .status(400)
      .send("Please make sure your email address is valid!");
  } else if (!body('password').isLength({ min: 5 })) {
    return response
      .status(400)
      .send(
        "Please make sure that your password is at least 5 characters long!"
      );
  }

  return next();
}
