FROM node:14.18-buster

RUN mkdir /app
WORKDIR /app

COPY package.json /app

RUN node -v 
RUN npm install

COPY . .
